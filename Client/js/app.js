var app = angular.module('app', ['ngRoute', 'ngResource', 'ngCookies']);

app.config( function ($routeProvider, $locationProvider) {
	$routeProvider.when("/", {
		controller: 'mainCont',
		templateUrl : "index.html"
	});
		$locationProvider.html5Mode(true);
	});

app.controller('mainCont', function($rootScope, $cookieStore, $scope, $route, $http) {
	$scope.nomeResponsavel = '';
	$scope.email = '';
	$scope.descricao = '';
	$scope.taskPendentes = [];
	$scope.taskConcluidas = [];
	$scope.taskSelectRetomar;
	$scope.passwordAuth = '';
	$http.get('/api/task')
		.then(function(response){
			if(response.data.length){
				for (let i = 0; i < response.data.length; i++) {
					if(response.data[i].status === 'pendente'){
						$scope.taskPendentes.push(response.data[i])
					}else{
						$scope.taskConcluidas.push(response.data[i])
					}
				}
			}
		}, function(response){
			console.log(response)
		});

	$scope.getAll = function (){
		$http.get('/api/task')
			.then(function(response){
				if(response.data.length){
					for (let i = 0; i < response.data.length; i++) {
						if(response.data[i].status === 'pendente'){
							$scope.taskPendentes.push(response.data[i])
						}else{
							$scope.taskConcluidas.push(response.data[i])
						}
					}
				}
			}, function(response){
				console.log(response)
			});
	}

	$scope.adicionarTask = function (){
		if($scope.nomeResponsavel === '' || $scope.email === '' || $scope.descricao === ''){
			window.alert("É necessário todos os campos para adicionar uma tarefa!");
			return false;
		}

		$http.post('/api/task',{
			email:$scope.email,
			nameresponsible: $scope.nomeResponsavel,
			description: $scope.descricao
		})
			.then(function(response){
				console.log(response)
				if(response.data.status && response.data.status === 400 && response.data){
					window.alert(response.data.data);
					return false;
				}else{
					if(response.data.status && response.data.status === 200){
						$scope.nomeResponsavel = '';
						$scope.email = '';
						$scope.descricao = '';
					}
				}
			}, function(response){
				console.log(response)
				// $scope.message = response;
			});
	}

	$scope.mudarStatus = function (task, alteracao){
		$http.put('/api/task',{
			id:task.id,
			status: alteracao
		})
			.then(function(response){
				if(response.data.status && response.data.status === 200){
					$scope.taskPendentes = [];
					$scope.taskConcluidas = [];
					$scope.getAll();
				}
			}, function(response){
				console.log(response)
			});
	}

	$scope.semTarefas = function (){
		$http.get('/api/task/empty')
			.then(function(response){
				if(response.data.status && response.data.status === 200){
					$scope.taskPendentes = [];
					$scope.taskConcluidas = [];
					$scope.getAll();
				}
			}, function(response){
				console.log(response)
			});
	}

	$scope.retomarTask = function (task){
		$scope.taskSelectRetomar = task;
	}

	$scope.autorizarPass = function (){
		if($scope.passwordAuth === 'TrabalheNaSaipos'){

			let valorReactive = $scope.taskSelectRetomar.reactive + 1;
			$http.put('/api/task/reactive',{
				id:$scope.taskSelectRetomar.id,
				status: 'pendente',
				reactive: valorReactive
			})
				.then(function(response){
					$scope.passwordAuth = '';
					if(response.data.status && response.data.status === 200){
						$scope.taskPendentes = [];
						$scope.taskConcluidas = [];
						$scope.getAll();
					}
				}, function(response){
					$scope.passwordAuth = '';
					console.log(response)
				});
		}else{
			window.alert("A senha está incorreta!");
			return false
		}
	}
})









