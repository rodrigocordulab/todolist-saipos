const express = require('express');
const bodyparser = require('body-parser');
const { sequelize } = require('./src/models');
const opn = require('opn');

const routes = require('./src/routes')
const path = require("path");

const app = express();


// parse body params and attache them to req.body
app.use(bodyparser.json());

app.use(
    bodyparser.urlencoded({
      parameterLimit: 100000,
      limit: "50mb",
      extended: true,
    })
);

app.use("/api",routes);
app.use('/Client', express.static(__dirname + '/Client'));

app.listen(3000, () => {
  console.log('Iniciado o servidor');
  sequelize.sync().then(() => {
    console.log('conectado')
  })

    app.get('/', function (req, res) {
        console.log(__dirname+'/Client/index.html')
        res.sendFile(path.join(__dirname+'/Client/index.html'));
    });

    opn('http://localhost:3000/');

})
