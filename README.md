# README #

Olá, primeiramente já agradeço pela oportunidade. Agora vamos direto para o código.

### O que é necessario? ###

* Node, minha máquina tem o v12.19.0
* PostgreSQL
* Um gerenciador de pacotes, utilizo o yarn
* Criar uma uma databse com o nome database_todosaipos no localhost
* Username utilizado -> postgres e senha -> masterkey
* Se necessário, só alterar no .env

### O que fazer para rodar? ###

* Apenar rodar o script start, no meu caso yarn start.
* Deverá abrir o link no http://localhost:3000/

### Fico a disposição ###

* Email: rodrigo.cordula@gmail.com
