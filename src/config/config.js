require('dotenv').config();

const {DB_HOST,DB_USERNAME,DB_PASSWORD} = process.env;

module.exports = {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": "database_todosaipos",
    "host": DB_HOST,
    "dialect": "postgres"
}
