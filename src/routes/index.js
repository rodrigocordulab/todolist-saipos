const express = require('express');

const taskRouter = require('../server/entities/task/task.router')

const router = express.Router();

router.use("/task", taskRouter);


module.exports = router ;
