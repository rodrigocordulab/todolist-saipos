// const Sequelize = require('sequelize')
const httpStatus = require('http-status');
const APIError = require('../../helpers/APIError')

const {Task, sequelize } = require('../../../models')

const Services = require('../../service')


const getAll = async (_req, res, next) => {
  try {
    const task = await Task.findAll({
      order: [
        ['id', 'DESC']
      ]
    })

    res.json(task)
  } catch (error) {
    return next(error)
  }
}

const create = async (req, res, next) => {
  let transaction;
  try {
    transaction = await sequelize.transaction();

    const {
      email,
      nameresponsible,
      description
    } = req.body;

    const verifEmail = await Services.getMailBoxLayer(email);

    if(verifEmail.data.did_you_mean !== ''){
      const error = new APIError(
        httpStatus.BAD_REQUEST,
        `Você quis digitar o email: ${verifEmail.data.did_you_mean}?`
      );
      await res.json({
        status: httpStatus.BAD_REQUEST,
        data: `Você quis digitar o email: ${verifEmail.data.did_you_mean}?`
      })

      return next(error);
    }

    const taskObj = {
      email,
      nameresponsible,
      description,
      status: 'pendente',
      reactive: 0
    }

    const taskCreate = await Task.create(taskObj, { transaction });

    if (taskCreate) {
        await transaction.commit();
        await res.json({
          status: httpStatus.OK,
          data: taskObj
        })
    }

  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
}

const editStatus = async(req, res, next) => {
  let transaction;
  try {
    transaction = await sequelize.transaction();

    const {id,status} = req.body;

    const reactiveFinded = await Task.findOne({
      where: {
        id
      }
    })

    if (!reactiveFinded) {
      const error = new APIError(
          httpStatus.NOT_FOUND,
          "Não foi encontrando nenhum dado com o id informado."
      );
      return next(error);
    }

    const taskObj = {
      email: reactiveFinded.email,
      description: reactiveFinded.description,
      nameresponsible: reactiveFinded.nameresponsible,
      status: status,
      reactive: reactiveFinded.reactive
    }

    await reactiveFinded.update(taskObj,{transaction})

    transaction.commit();
    res.json({
      status: httpStatus.OK,
      message: "Dados atualizados com sucesso!"
    })

  } catch (error) {
    transaction.rollback();
    next(error)
  }
}

const getReactive = async (req, res, next) => {

  try {
    const {
      id
    } = req.body;

    const taskFinded = await Task.findOne({
      where: {
        id
      },
      attributes: ['reactive']
    })

    const retorn = {
      reactive: taskFinded.reactive
    }

    res.json(retorn);
  } catch (error) {
    return next(error)
  }

}

const putReactive = async (req,res,next)=>{
  let transaction;
  try {
    transaction = await sequelize.transaction();

    const {reactive, id,status} = req.body;

    const reactiveFinded = await Task.findOne({
      where: {
        id
      }
    })

    if (!reactiveFinded) {
      const error = new APIError(
        httpStatus.NOT_FOUND,
        "Não foi encontrando nenhum dado com o id informado."
      );
      return next(error);
    }

    const taskObj = {
      email: reactiveFinded.email,
      description: reactiveFinded.description,
      nameresponsible: reactiveFinded.nameresponsible,
      status: status,
      reactive: reactive
    }

    await reactiveFinded.update(taskObj,{transaction})

    transaction.commit();
    res.json({
      status: httpStatus.OK,
      message: "Dados atualizados com sucesso!"
    })

  } catch (error) {
    transaction.rollback();
    next(error)
  }
}

const getEmpty = async (req, res, next) => {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const facts = await Services.getCatsFacts();

    const array = []

    for (let index = 0; index < 3; index++) {

      const text = `${facts.data[index].text}`;
      const obj = {
        nameresponsible:'Eu',
        email: 'eu@me.com',
        status: 'pendente',
        reactive: 0,
        description:text
      }
      array.push(obj);
      await Task.create(obj, { transaction });
    }

    transaction.commit()
    res.json({
      status: httpStatus.OK,
      retorno: array
    })

  } catch (error) {
    transaction.rollback();
    next(error)
  }
}


module.exports = {
  getAll,
  create,
  editStatus,
  putReactive,
  getReactive,
  getEmpty
}
