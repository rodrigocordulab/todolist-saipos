const express = require('express')
const router = express.Router()

const taskController = require('./task.controller')


//route /api/task/
router.route("/")
  .get(taskController.getAll)
  .post(taskController.create)
  .put(taskController.editStatus)

//route /api/task/reactive
router.route("/reactive")
  .get(taskController.getReactive)
  .put(taskController.putReactive)

//route /api/task/empty
router.route("/empty")
  .get(taskController.getEmpty)


  module.exports = router;
