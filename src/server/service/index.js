const api = require('./api')

const Services = {
  getCatsFacts: async () =>{
    try {
      return await api.get('https://cat-fact.herokuapp.com/facts');
    } catch (error) {
      return error.response;
    }
  },
  getMailBoxLayer: async (data) =>{
    try {
      return await api.get(`https://apilayer.net/api/check?access_key=c8f6523a4ca38abb41f0d566b49f6176&email=${data}`)
    } catch (error) {
      return error.response
    }
  }
}

module.exports = Services;
