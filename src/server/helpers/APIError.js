/**
 * @extends Error
 */
class APIError extends Error {
    constructor(status = 200, ...params) {
        // Pass remaining arguments (including vendor specific ones) to parent constructor
        super(...params);

        // Maintains proper stack trace for where our error was thrown (only available on V8)
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, APIError);
        }

        this.name = "APIError";
        this.status = status;
        this.date = new Date();
    }
}

module.exports = APIError;
